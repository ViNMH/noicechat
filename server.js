var http = require('http');

server_app = http.createServer(requestlistener);

var fs = require('fs');
var io = require('socket.io')(server_app);
var util = require('./util');

var connected_users = [];
var last_messages = [];

function updateMessage(socket_obj, message, message_class) {
    var msg_obj = {
        msg: message,
        class: message_class
    };

    if (last_messages.length > 10) {
        last_messages.shift();
    }

    last_messages.push(msg_obj);
    socket_obj.emit("update messages", msg_obj);

}

function requestlistener(request, response) {
    var filename = "";
    if (request.url == "/") {
        filename = __dirname + '/static/html/index.html';
    } else {
        filename = __dirname + request.url;
    }

    fs.readFile(filename, function (err, data) {
        if (err) {
            response.writeHead(404);
            response.end('Error loading file');
        } else {
            response.writeHead(200);
            response.end(data);
        }
    });
}

io.on("connection", function (socket) {
    socket.on("login", function (nickname, callback) {
        if (!(nickname in connected_users) && nickname != "all") {
            socket.nickname = nickname;
            connected_users[nickname] = socket;

            for (i in last_messages) {
                socket.emit("update messages", last_messages[i]);
            }

            io.sockets.emit("update users", Object.keys(connected_users));
            updateMessage(io.sockets, util.get_current_date() + nickname + " has joined us...", "system");
            console.log('User ' + nickname + ' Connected!');
            callback(true);
        } else {
            callback(false);
        }
    });
    socket.on("send message", function (data, callback) {
        var sent_message = data.msg;
        var users = data.usrs;
        if (users == null || users.includes("all"))
            users = [];

        sent_message = util.get_current_date() + socket.nickname + ': ' + sent_message;

        if (users.length == 0) {
            updateMessage(io.sockets, sent_message, "");
        } else {
            socket.emit("update messages", {
                msg: sent_message,
                class: "private"
            });
            for (i in users) {
                connected_users[users[i]].emit("update messages", {
                    msg: sent_message,
                    class: "private"
                });
            }
        }

        callback()
    });
    socket.on("disconnect", function () {
        delete connected_users[socket.nickname];
        io.sockets.emit("update users", Object.keys(connected_users));
        updateMessage(io.sockets, util.get_current_date() + socket.nickname + " has left us...", "system");
    });
})

server_app.listen(8000);