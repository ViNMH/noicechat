module.exports = {
    get_current_date: function(){
        var date = new Date();
        var day = (date.getDate()<10 ? '0' : '') + date.getDate();
        var month = ((date.getMonth() + 1)<10 ? '0' : '') + (date.getMonth() + 1);
        var year = date.getFullYear();
        var hour = (date.getHours()<10 ? '0' : '') + date.getHours();
        var minute = (date.getMinutes()<10 ? '0' : '') + date.getMinutes();
        var second = (date.getSeconds()<10 ? '0' : '') + date.getSeconds();

        var formatted_date = year + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
        return '[' + formatted_date + '] ';
    }
}