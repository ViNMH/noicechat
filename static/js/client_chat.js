var socket = io.connect();

$("form#chat").submit(function (e) {
    e.preventDefault();

    var message = $(this).find("#message_text").val();
    var users = $("#user_list").val();

    socket.emit("send message", {msg: message, usrs: users}, function () {
        $("form#chat #message_text").val("");
    });
});

socket.on("update messages", function (data) {
    var formatted_message = $("<p />").text(data.msg).addClass(data.class);
    $("#chat_history").append(formatted_message);
});

$("form#login").submit(function (e) {
    e.preventDefault();
    socket.emit("login", $(this).find("#nickname").val(), function (valid) {
        if (valid) {
            $("#login_screen").hide();
            $("#chat_screen").show();
        } else {
            $("#login_screen").val("");
            alert("Username already present");
        }
    });
});

socket.on("update users", function (users) {
    $("#user_list").empty();
    $("#user_list").append("<option value='all'>All</option>");
    $.each(users, function (index) {
        var user_option = $("<option />").text(users[index]);
        $("#user_list").append(user_option);
    });
});